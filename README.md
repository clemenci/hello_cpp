This is a small example project meant to test build systems.

To build invoke

```
cmake -S . -B build
cmake --build build
```

then run

```
./build/hello_cpp
```

or install with (if you have the rights)

```
sudo cmake --install build
```
